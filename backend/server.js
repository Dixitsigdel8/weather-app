const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config()
const app = express()
const PORT = process.env.PORT || 5000
const authRoutes = require('./routes/auth')
const userRoutes = require('./routes/user')

require('./models/user')

//KmaLeJh5Uj3Q3xnS


mongoose.connect(process.env.MONGOURL,{useNewUrlParser:true,useUnifiedTopology:true})
const db = mongoose.connection;
db.on('error',(err)=>{
    console.log('error is connection !!',err)
})
db.once('connected',()=>{
    console.log('Database is connected !!!!')
})

//middleware
app.use(express.json())

//api routes
app.use(authRoutes)
app.use(userRoutes)

app.listen(PORT,()=>{
    console.log(`Server is running at ${PORT}`)
})