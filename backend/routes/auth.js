const express = require('express')
const router = express.Router()
const bcrypt=require('bcryptjs')
const jwt=require('jsonwebtoken')
// const mongoose = require('mongoose')
// const User = mongoose.model('User')
const User = require('../models/user')




router.post('/signup',(req,res) =>{
    const {name,email,password}=req.body

   if(!email || !name || !password){
       return res.status(500).json({error:"input empty"})
   }
   User.findOne({email:email})
   .then((savedUser)=>{
       if(savedUser){
         return  res.status(400).json({msg:"User already exist"})
       }
       bcrypt.hash(password,12)
       .then(hashedpassword=>{
        const user=new User({
            email,name,password:hashedpassword
        })
        user.save()
        .then(user=>{
            res.status(200).json({msg:"saved successfully"})
        }).catch(error=>{
           console.log(error)
        })

       })
      
   }).catch(error=>{
       console.log(error)
   })
})

router.post('/signin',(req,res)=>{
    const{email,password}=req.body

    if(!email || !password){
        return res.status(400).json({msg:"Empty field email password"})
    }
    User.findOne({email:email})
    .then((savedUser)=>{
        if(!savedUser){
            res.status(400).json({error:"Invalid email and password"})
        }
        bcrypt.compare(password,savedUser.password)
        .then(doMatch=>{
            if(doMatch){

                const token=jwt.sign({_id:savedUser._id},process.env.JWT_SECRET)
                const {_id,name,email}=savedUser
                res.status(200).json({token,user:{_id,name,email}})


                //res.status(200).json({msg:"Sign in"})
            }else{
                res.status(400).json({error:"Invalid email and password"})
            }
        }).catch(error=>{
            console.log(error)
        })

    }).catch(error=>{
        console.log(error)
    })
})

module.exports=router