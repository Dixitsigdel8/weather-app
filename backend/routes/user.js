const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const requirelogin = require("../middleware/requirelogin");
const User = require("../models/user");

router.get("/user/:id", requirelogin, (req, res) => {
  User.findOne({ _id: req.params.id })
    .select("-password")
    .then((user) => {
      
      res.json({ user});
    })
    .catch((err) => {
      return res.status(400).json({ err: "User not found" });
    });
});

module.exports = router;
