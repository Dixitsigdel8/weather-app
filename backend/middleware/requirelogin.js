const jwt = require('jsonwebtoken')
// const mongoose = require('mongoose')
// const User = mongoose.model('User')
const User = require('../models/user')
module.exports = (req,res,next) => {
    const {authorization} = req.headers
    if(!authorization) {
        return res.status(400).json({error:"You must logged in !! "})

    }
    const token = authorization.replace("Bearer ","")
    jwt.verify(token,process.env.JWT_SECRET,(error,payload)=>{
        if(error) {
            return res.status(400).json({error:"Must logged in"})
        }
        const {_id}=payload
        User.findById(_id)
        .then(userdata=>{
            req.user = userdata
            next()
        })
    })
}