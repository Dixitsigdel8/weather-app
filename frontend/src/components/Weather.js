import React, { useEffect, useState } from "react";
import DisplayWeather from "./DisplayWeather";
import "./weather.css";

const Weather = ({history}) => {
  const APIKEY = "27d2eecee40e1e48482b8052dc35ebbe";

  useEffect(() => {
    if(!localStorage.getItem("authToken")) {
      history.push("/login")
    }
    const fetchPrivateData = async () => {
      const config = {
        headers: {
          "Content-Type":"application/json",
          Autorization:localStorage.getItem("authToken")
        }
      }
    }
    fetchPrivateData();
  },[history])


const logoutHandler = () => {
  localStorage.removeItem("authToken");
  history.push("/login")
}






  const [form, setForm] = useState({
    city: "",
    country: "",
  });
  const [weather, setWeather] = useState([]);
  async function weatherData(e) {
    e.preventDefault();
    if (form.city == "") {
      alert("Add values");
    } else {
      const data = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${form.city},${form.country}&appid=${APIKEY}`
      )
        .then((res) => res.json())
        .then((data) => data);
        setWeather(
            {
                data:data
            }
        )
    }
  }
  const handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;

    if (name === "city") {
      setForm({ ...form, city: value });
    }
    if (name === "country") {
      setForm({ ...form, country: value });
    }
  };
  return (
    <div className="weather">
      <span className="title">Weather App</span>
      Weather
      <br />
      <button onClick={logoutHandler}></button>
      <form>
        <input
          type="text"
          name="city"
          placeholder="city"
          onChange={(e) => handleChange(e)}
        />
        &nbsp; &nbsp; &nbsp;
        <input
          type="text"
          name="country"
          placeholder="country"
          onChange={(e) => handleChange(e)}
        />
        <button className="getweather" onClick={(e) => weatherData(e)}>
          Submit
        </button>
      </form>
      {
          weather.data != undefined ?
            <div>
                <DisplayWeather data={weather.data} />
                </div>
          : null
      }
    </div>
  );
};

export default Weather;

//27d2eecee40e1e48482b8052dc35ebbe
