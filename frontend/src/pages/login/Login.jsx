import React,{useState,useEffect} from "react";
import axios from 'axios';
import "./login.css";

const Login = ({history}) => {
  const [email,setEmail] = useState("")
  const [password,setPassword]=useState("")
  const [error,setError]=useState("")
  useEffect(() => {
    if(localStorage.getItem("authToken")) {
      history.pushState("/")
    }
  },[history])
  const loginHandler = async(e) => {
    e.preventDefault()
    const config = {
      header:{
        "Content-Type":"application/json",
      }
    }
    try {
      const {data} = axios.post(
        "/signin",
        {email,password},
        config
      )
      localStorage.setItem("authToken",data.token);
      history.push("/")
    } catch (error) {
      setError(error.response.data.error);
      setTimeout(() => {
        setError("")
      },5000)
      
    }
  }
  return (
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="loginLogo">Weather Application</h3>
          <span className="loginDesc">
           Best weather application 
          </span>
        </div>
        <div className="loginRight">
          <div className="loginBox">
            <input placeholder="Email" className="loginInput" />
            <input placeholder="Password" className="loginInput" />
            <button className="loginButton" onClick={loginHandler}> Log In </button>
            <span className="loginForgot">Forgot Password ?</span>
            <button className="loginRegisterButton">
              Create a New Account
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
