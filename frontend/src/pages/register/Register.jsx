import React,{useState} from "react";
import axios from 'axios'
import "./register.css";
import {Link} from 'react-router-dom'
const Register = ({history}) => {
  const [username,setUsername] =useState("")
  const [email,setEmail]=useState("")
  const [password,setPassword]=useState("")
  const [confirmPassword,setConfirmPassword]=useState('')
  const [error,setError]=useState('')
  const registerHandler=async(e) =>{
    e.preventDefault()
    const config = {
      header:{
        "Content-Type":"application/json"
      }
    }
    if(password !== confirmPassword) {
      setPassword("")
      setConfirmPassword("")
      setTimeout(() => {
            setError("")
      },5000)
      return setError("Password do not match")

    }
    try {
      const {data} = await axios.post("/signup",{username,email,password},config);
      history.push("/login")
    } catch (error) {
      setError(error.response.data.error);
      setTimeout(() => {
        setError("")
      },5000)
    }
  }
  return (
    <div className="login">
      <div className="loginWrapper">
        <div className="loginLeft">
          <h3 className="loginLogo">Weather Application</h3>
          <span className="loginDesc">
           Best weather application
          </span>
        </div>
        {error && <span>{error}</span> }
        <div className="loginRight">
          <div className="loginBox">
            
            <input placeholder="Username" className="loginInput" required value={username} onChange={(e) => setUsername(e.target.value)} />
            <input placeholder="Email" className="loginInput" required value={email} onChange={(e) => setEmail(e.target.value)} />
            <input placeholder="Password" className="loginInput" required value={password} onChange={(e) => setPassword(e.target.value)} />
            <input placeholder="Password Again" className="loginInput" required value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
            <button className="loginButton" onClick={registerHandler}> Sign Up </button>
            <Link to="/login">
             <button className="loginRegisterButton">
              Log into Account
            </button>
            </Link>
           
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
