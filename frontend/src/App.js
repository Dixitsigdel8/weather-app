
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
   
} from "react-router-dom";
import Weather from './components/Weather';
import Login from './pages/login/Login'
import Register from './pages/register/Register';

//routing
import PrivateRoute from './components/PrivateRoute';
function App() {
  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/" component={Weather} />
        {/* <Route exact path="/" component={Weather} /> */}
         
        <Route exact path="/login" component={Login} />

        <Route exact path="/register" component={Register} />
       
      </Switch>
    </Router>
    
    
  );
}

export default App;
